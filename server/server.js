const http = require("http");

const PORT = 3000;
const HOST = '0.0.0.0';

const requestListener = function (req, res) {
    res.setHeader("Content-Type", "application/json");
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Headers', 'origin, content-type, accept');

    res.writeHead(200);
    res.end(
        JSON.stringify({ date: new Date().toISOString() })
    );
};


const server = http.createServer(requestListener);



server.listen(PORT, HOST, () => {
    console.log(`Server is running on http://${HOST}:${PORT}`);
});

