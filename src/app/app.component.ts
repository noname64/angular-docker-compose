import { Component } from '@angular/core';
import { DateService } from './services/date.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public date: string;
  public readonly title = 'test-angular-docker-compose';

  constructor (
    private readonly dateService: DateService,
  ) {}

  public ngOnInit(): void {
    this.dateService.getDate().subscribe((response) => {
      this.date = response.date;
    });
  }
}
