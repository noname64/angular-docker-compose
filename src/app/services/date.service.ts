import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

const endpoint = 'http://0.0.0.0:3000/';
const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

interface DateResponse {
  date: string;
}

@Injectable({
  providedIn: 'root'
})
export class DateService {
  constructor(
    private readonly http: HttpClient,
  ) { }

  getDate(): Observable<DateResponse> {
    return this.http.get<DateResponse>(endpoint, httpOptions);
  }

}
